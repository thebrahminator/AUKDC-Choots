import urllib.request
import requests

BASE_URL = 'https://www.aukdc.edu.in/photos/'
while True:
    for i in range(1,8):
        roll_number_1 = '201'+str(i)+'103'
        for j in ['0', '5']]:
            roll_number_2 = roll_number_1+str(j)
            for k in range(0,10):
                roll_number_3 = roll_number_2+str(k)
                for l in range(0,10):
                    roll_number_4 = roll_number_3+str(l)
                    print(roll_number_4, '\n')
                    query_url = BASE_URL + str(roll_number_4) + '.jpg'
                    storage_file = './images/'+query_url
                    r = requests.get(query_url)
                    if r.status_code != 200:
                        continue
                    urllib.request.urlretrieve(query_url, storage_file)